source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?('/')
  "https://github.com/#{repo_name}.git"
end

gem 'sqlite3' # data storage
gem 'capybara' # browser interaction simulation
# gem 'selenium-webdriver' # use Firefox for browser interaction
gem 'capybara-webkit' # headless browser interaction, based on WebKit

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: %i[mri mingw x64_mingw]
  # specs/testing:
  gem 'rspec', require: false
end

group :development do
  # continuous testing:
  gem 'guard', require: false
  # mutation testing
  gem 'mutant-rspec', require: false # https://github.com/mbj/mutant
  # gem validation
  gem 'bundler-audit', require: false # https://github.com/postmodern/bundler-audit
  # code analysis tools:
  gem 'rubocop', require: false # https://github.com/bbatsov/rubocop
  gem 'rubycritic', require: false # https://github.com/whitesmith/rubycritic
end

group :test do
  # code test coverage analysis:
  gem 'simplecov', require: false
end
