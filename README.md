# Data Extractor for the City of Calgary official Candidate Profiles

This is a quck-and-dirty script for extracting the candidate profile data from the [City of Calgary’s official candidate profiles for the 2017 municipal election](http://www.calgary.ca/election/Pages/meet-the-candidates/default.aspx). (Yes, it’s a very bad coding practice to dump multiple classes into one file — and I’ve dumped _everything_ into just one script file in this case. I did say “quick-and-dirty”.)

You don’t need to run it yourself since the data has already been compiled to a [CSV file hosted on the Calgary Democracy website](http://calgarydemocracy.ca/levels/calgary/elections/2017/official-profiles) (not affiliated with the City of Calgary). This code is made available to hopefully help with similar data extraction challenges in the future.

## Requirements / Installation

qt is required for the capybara-webkit driver. To install it on Mac (assuming you have [Homebrew](https://brew.sh/) installed):
    brew install qt@5.5
(The `@5.5` is necessary to prevent a higher version from being used because the code libraries used here are not compatible with later versions of qt.)

Then make sure the qt directory is in the `PATH` by adding this line to `.bashrc`:
    export PATH="$(brew --prefix qt@5.5)/bin:$PATH"

You’ll have to do a `bundle install`, as is normal for Ruby projects, to install the gem libraries this script relies on.

If that’s all in the “happy case”, you should be able to run the script with just:
    ./dataextractor.rb
(assuming your current working directory is the project directory for this code.)

## A complaint about the City’s profile data

The reason I had to go to all the trouble (and it was a notable amount of trouble — many hours of coding and testing) of writing up this script is that there was significant obfuscation of the candidate profile data on the City’s website.

1. First, each candidate profile (131 in all) is on a separate web page.
2. Second, the actual html for each profile page doesn’t contain any of the data. Instead, it is all loaded via Javascripts (of which there are countless per page.
3. Third, some of the content is truncated by default (platform and bio) necessitating the user to click on pseudo-links to trigger the Javascript actions to display the full content.

I manually extracted the links to each of the individual candidate profile pages, embedding those links as one giant array in the script. (This could have been automated, but since it was a relatively minor task — no more than 5 minutes — I just did it by hand.)

After figuring out that the content wasn’t actually contained in the html of the webpages, I decided to make use of Capybara, since I have some familiarity with it from doing automated feature tests for some of my web coding projects. Capybara’s default mode (`:rack_test`) doesn’t handle Javascript, but one can add other drivers which can. I elected to use the `capybara-webkit` library because, unlike Selenium (another common driver for capybara), it can run headless (meaning it doesn’t rely on a separate web-browser to run).

Then I had to fiddle around, figuring out how to get the Capybara stuff installed. and working as I wanted, outside of a testing environment. As is seemingly always the case with these sorts of things, there was much trial and error until I got it all figured out.

In the end, though, I now have a template for future scripts like these. So, it’s perhaps not quite as frustratingly big of a waste of time as it felt like during the process. 😆

## Credits

Written by [Grant Neufeld](http://grantneufeld.ca/), on September 20, 2017, for the [Calgary Democracy](http://calgarydemocracy.ca/) project.
