#!/usr/bin/env ruby

require 'byebug'
require 'capybara'
require 'capybara/dsl'
require 'capybara-webkit'
require 'sqlite3'

Capybara.default_driver = :webkit

class Extractor
  include Capybara::DSL

  def initialize(logger: nil)
    @logger = logger
  end

  def profile_from_url(url)
    profile = Profile.new
    profile.url = url
    visit url
    profile.raw = page.body
    # get the office
    profile.office = try_to_get_text_content('eleWardAndType')
    # get the full name
    profile.name = try_to_get_text_content('eleFullName')
    # get the image url
    img = try_to_get_src('elePhoto')
    # ignore default image
    profile.img = img unless img == '/election/Scripts/election/images/candidates/person.svg'
    # expand the platform
    try_to_click_selector('#ePlatform a[class="ele-platform-btn-read-more"]')
    # get the platform
    platform_first = get_html_content('elePlatform')
    platform_more = get_html_content('elePlatformMore')
    profile.platform = platform_first + platform_more
    # get the vid link
    profile.vid = try_to_get_link('eleVideoLink')&.sub(/&rel=[0-9]+\z/, '')
    # expand the bio
    try_to_click_selector('#eBiography a[class="ele-bio-btn-read-more"]')
    # get the bio
    bio_first = get_html_content('eleBiography')
    bio_more = get_html_content('eleBiographyMore')
    profile.bio = bio_first + bio_more
    # social media
    all('#ele-social-columns a').each { |a| profile.extra_links[a.text] = a[:href] }
    profile.recognize_links
    # contact info
    profile.phone = try_to_get_text_from_selector('#ele-contact-columns a[title="Phone"]')
    profile.email = try_to_get_text_from_selector('#ele-contact-columns a[title="Email"]')
    url = try_to_get_text_from_selector('#ele-contact-columns a[title="Website"]')
    # ensure trailing slash for web_url
    url = url + '/' if url && url != '' && !url.match(/\A[a-z_]+[:\/]+[^\/]+\/.*/)
    profile.web_url = url
    profile
  end

  protected

  def log(message)
    @logger.log(message) if @logger
  end

  def try_to_click_selector(selector)
    # platform_read_more = find('#ePlatform a[class="ele-platform-btn-read-more"]')
    # platform_read_more.click if platform_read_more
    find(selector)&.click
  rescue Capybara::ElementNotFound => err
    log("ERROR #{err}")
    nil
  end

  def try_to_get_text_content(element_id)
    try_to_get_text_from_element(get_element(element_id))
  rescue Capybara::ElementNotFound => err
    log("ERROR #{err}")
    ''
  end

  def try_to_get_text_from_selector(selector)
    try_to_get_text_from_element(find(selector))
  rescue Capybara::ElementNotFound => err
    log("ERROR #{err}")
    ''
  end

  def try_to_get_text_from_element(element)
    element&.text
  rescue Capybara::ElementNotFound => err
    log("ERROR #{err}")
    ''
  end

  def get_html_content(element_id)
    result = page.evaluate_script("document.getElementById('#{element_id}').innerHTML")
    # strip leading or trailing whitespace
    result = result&.strip
    # convert space runs and linebreaks to single spaces
    result.gsub(/[ \r\n\t]*[\r\n][ \r\n\t]*/, ' ')
  end

  def try_to_get_link(element_id)
    get_element(element_id)[:href]
  rescue Capybara::ElementNotFound => err
    log("ERROR #{err}")
    ''
  end

  def try_to_get_src(element_id)
    get_element(element_id)[:src]
  rescue Capybara::ElementNotFound => err
    log("ERROR #{err}")
    ''
  end

  def get_element(element_id)
    find("##{element_id}")
  end
end

class Profile
  attr_accessor(
    :url, :office, :name, :img, :platform, :vid, :bio,
    :facebook, :instagram, :linkedin, :twitter, :youtube,
    :phone, :email, :web_url,
    :retrieved_at, :extra_links, :raw
  )

  def initialize
    @url = ''
    @office = ''
    @name = ''
    @img = ''
    @platform = ''
    @vid = ''
    @bio = ''
    @facebook = ''
    @instagram = ''
    @linked_in = ''
    @twitter = ''
    @youtube = ''
    @phone = ''
    @email = ''
    @web_url = ''
    @retrieved_at = ''
    @extra_links = {}
    @raw = ''
  end

  def links_to_s
    chunks = []
    extra_links.each { |key, link| chunks << "{#{key}:#{link}}" }
    chunks.join(',')
  end

  def links_from_s(links_str)
    link_parts = links_str.scan(/\{([^\{\}:]+):([^\{\}]+)\}/)
    link_parts.each { |part| extra_links[part[0]] = part[1] }
  end

  def recognize_links
    extra_links.each do |key, link|
      case key
      when 'Facebook'
        # strip extraneous url cruft
        link.sub!(/\?ref=.*\z/, '')
        link.sub!(/\#.*\z/, '')
        self.facebook = link
        extra_links.delete(key)
      when 'Instagram'
        self.instagram = link
        extra_links.delete(key)
      when 'LinkedIn'
        self.linkedin = link
        extra_links.delete(key)
      when 'Twitter'
        self.twitter = link
        extra_links.delete(key)
      when 'Youtube'
        # strip extraneous url cruft
        link.sub!(/\??view_as=.+\z/, '')
        self.youtube = link
        extra_links.delete(key)
      end
    end
  end

  def diff(other)
    diffs = []
    diffs << 'url' unless url == other.url
    diffs << 'office' unless office == other.office
    diffs << 'name' unless name == other.name
    diffs << 'img' unless img == other.img
    diffs << 'platform' unless platform == other.platform
    diffs << 'vid' unless vid == other.vid
    diffs << 'bio' unless bio == other.bio
    diffs << 'facebook' unless facebook == other.facebook
    diffs << 'instagram' unless instagram == other.instagram
    diffs << 'linkedin' unless linkedin == other.linkedin
    diffs << 'twitter' unless twitter == other.twitter
    diffs << 'youtube' unless youtube == other.youtube
    diffs << 'phone' unless phone == other.phone
    diffs << 'email' unless email == other.email
    diffs << 'web_url' unless web_url == other.web_url
    diffs
  end
end

class Datastore
  attr_reader :db

  FILEPATH = 'datastore.db'.freeze

  def initialize
  end

  def self.open
    new.open_db
  end

  def open_db
    # if not exists
    @db =
      if File.exists?(FILEPATH)
        SQLite3::Database.open FILEPATH
      else
        SQLite3::Database.new FILEPATH
      end
    self
  end
end

# Abstract class
# Subclasses must implement:
# - table_name
# - create_table_sql
# - key_field
# Subclasses may implement:
# - default_order
class Table
  attr_reader :db

  def self.open(database)
    new(database)
  end

  def initialize(database)
    @db = database
    ensure_table
  end

  def ensure_table
    db.execute(create_table_sql) unless exists?
    self
  end

  def all
    db.execute("SELECT * FROM #{table_name} #{order}")
  end

  def get_row(key)
    db.execute("SELECT * from #{table_name} WHERE #{key_field} = ? #{order} LIMIT 1", key).first
  end

  protected

  def order
    "ORDER BY #{default_order}" if default_order
  end

  def default_order
    nil
  end

  def exists?
    info = db.table_info(table_name)
    info.size > 0
  end
end

class ProfileTable < Table
  def get_profile(url)
    row = get_row(url)
    row_to_profile(row) if row
  end

  def row_to_profile(row)
    profile = Profile.new
    profile.url = row[0]
    profile.office = row[1]
    profile.name = row[2]
    profile.img = row[3]
    profile.platform = row[4]
    profile.vid = row[5]
    profile.bio = row[6]
    profile.facebook = row[7]
    profile.instagram = row[8]
    profile.linkedin = row[9]
    profile.twitter = row[10]
    profile.youtube = row[11]
    profile.phone = row[12]
    profile.email = row[13]
    profile.web_url = row[14]
    profile.links_from_s(row[15])
    profile.raw = row[16]
    profile.retrieved_at = Time.at(row[17])
    profile
  end

  def save_profile(profile)
    sql = <<-SQL
      INSERT INTO profiles (
        url, office, name, img, platform, vid, bio,
        facebook, instagram, linkedin, twitter, youtube,
        phone, email, web_url, extra_links,
        raw, retrieved_at
      ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);
    SQL
    db.execute(
      sql,
      profile.url, profile.office, profile.name, profile.img, profile.platform,
      profile.vid, profile.bio, profile.facebook, profile.instagram, profile.linkedin,
      profile.twitter, profile.youtube, profile.phone, profile.email, profile.web_url,
      profile.links_to_s, profile.raw, Time.now.to_i
    )
  end

  protected

  def table_name
    'profiles'
  end

  def key_field
    'url'
  end

  def default_order
    'retrieved_at DESC'
  end

  def create_table_sql
    <<-SQL
      create table profiles (
        url VARCHAR(127),
        office VARCHAR(127),
        name VARCHAR(127),
        img VARCHAR(127),
        platform TEXT,
        vid VARCHAR(63),
        bio TEXT,
        facebook VARCHAR(127),
        instagram VARCHAR(127),
        linkedin VARCHAR(127),
        twitter VARCHAR(127),
        youtube VARCHAR(127),
        phone VARCHAR(31),
        email VARCHAR(63),
        web_url VARCHAR(127),
        extra_links TEXT,
        raw TEXT,
        retrieved_at INTEGER
      );
    SQL
  end
end

class Logger
  attr_reader :file
  LOGPATH = 'extractor.log'.freeze

  def initialize
    @file = File.open(LOGPATH, 'a')
  end

  def log(text)
    file.write("#{text}\n")
  end

  def done
    file.close
  end
end

# ======================================================================

url_list = [
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=AdamBoechler',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=AdamFrisch',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=AlexColumbos',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=AltheaAdams',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=AndreChabot',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=AntonioBalangue',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=ArtJohnston',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=AryanSadat',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=BalrajNijjar',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=BiancaSmetacek',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=BillSmith',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=BlairBerdusco',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=BossMadimba',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=BradCunningham',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=BrentAlexander',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=BrentChisholm',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=CamKhan',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=CarlaObuck',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=CarterThomson',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=CathieWilliams',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=CesarSaavedra',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=CherylLink',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=CherylLow',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=ChrisBlatch',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=ChrisDavis',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=ChristopherMaitland',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=ChristopherMcMillan',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=ColeChristensen',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=ConnieHamilton',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=CoralBlissTaylor',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=CurtisOlson',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=DavidLapp',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=DavidMetcalfe',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=DavidWinkler',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=DeanBrawn',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=DianeColleyUrquhart',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=DruhFarrell',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=EmileGabriel',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=EsmahanRazavi',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=EvanWoolley',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=FaithGreaves',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=GarGar',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=GeorgeChahal',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=GeorgeGeorgeou',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=Gian-CarloCarra',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=GordCummings',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=GraceNelson',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=GregMiller',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=GurpreetGrewal',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=HermannMuller',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=HirdeJassal',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=IanMcAnerin',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=IssaMosa',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=JameelaGhann',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=JanetEremenko',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=JasbirChahal',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=JasonAchtymichuk',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=JeffBrownridge',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=JeffDavison',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=JenniferSeamone',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=JenniferWyness',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=JeromyFarkas',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=JoeMagliocca',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=JulieHrdlicka',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=JunLin',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=JyotiGondek',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=KamillaPrasad',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=KarenDraper',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=KarlaCharest',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=KayAdeniyi',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=KeithSimmons',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=KelashKumar',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=KennethDoll',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=KimTyers',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=LarryHeather',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=LauraHack',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=LindaJohnson',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=LindaWellman',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=LisaDavis',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=LoryIovinelli',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=MackenzieQuigley',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=MarekHejduk',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=MargotAftergood',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=MarilynDennis',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=MarioDeshaies',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=MarkDyrholm',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=MaryMartin',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=MerleTerlesky',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=MichelleRobinson',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=MikeBradshaw',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=MuzaffarAhmad',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=MyraDsouza',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=NaheedNenshi',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=NajeebButt',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=NimraAmjad',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=NumanElhussein',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=OmarMkeyo',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=PamelaKing',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=PamelaRath',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=PatriciaBolger',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=PeterDemong',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=RamanGill',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=RayJones',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=RekhaDhawan',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=RichardHehr',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=RobertDickinson',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=SabrinaBartlett',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=SadiqValliani',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=SalimahKassam',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=SamanthaMacleod',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=SanjeevKad',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=SaraPeden',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=SarbdeepBaidwan',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=SeanChu',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=SeanYost',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=ShaneKeating',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=ShaunaEldan',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=SherrisaCelis',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=SohailSherwani',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=SrinivasGanti',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=StantheManWaciak',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=SteveTurner',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=SyedHasnain',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=TeresaHargreaves',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=ToryTomblin',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=TrevorBuckler',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=TrinaHurdman',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=TudorDinca',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=VeerpalRai',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=WardSutherland',
  'http://www.calgary.ca/election/Pages/meet-the-candidates/profile.aspx?profileId=WasimaSultan'
]

start_time = Time.now

logger = Logger.new

database = Datastore.open
db = database.db
profiles = ProfileTable.open(database.db)
extractor = Extractor.new(logger: logger)

# Extract all the profile data from the remote host into our local db
first = true
url_list.each do |url|
  time_check = Time.now
  if first
    first = false
  else
    # pause between page loads to be respectful toward the remote server(s)
    sleep(5 + (rand(500) * 0.01)) # between 5 and 10 seconds
  end
  puts "URL: #{url}"
  # get the previous version from the database, if any
  previous = profiles.get_profile(url)
  # extract the profile from the remote page
  profile = extractor.profile_from_url(url)
  # compare with previous
  diffs = previous.nil? ? [] : profile.diff(previous)
  # if new or changed
  if previous.nil? || diffs.size > 0
    # save the new profile
    profiles.save_profile(profile)
    # note the changes
    extras = []
    profile.extra_links.each { |key, link| extras << "#{key}: #{link}" }
    action_label = previous.nil? ? 'PROFILE' : 'CHANGED'
    log_msg = ["#{action_label} #{url}", diffs.join(', '), extras.join("\n\t")]
      .delete_if { |value| value.nil? || value == '' }
      .join("\n\t")
  else
    log_msg = "UNCHANGED #{url}"
  end
  logger.log(log_msg)
  puts "\t#{Time.now - time_check} seconds"
end

logger.done

time_taken = Time.now - start_time
minutes = time_taken.to_i / 60
seconds = time_taken % 60
puts "TOTAL TIME: #{minutes} minutes, #{seconds} seconds"

# To export to a CSV file:
#   sqlite3 -header -csv datastore.db "SELECT * FROM profiles;" > profiles.csv
# or:
#   sqlite3 -header -csv datastore.db "SELECT url, office, name, img, platform, vid, bio, facebook, instagram, linkedin, twitter, youtube, phone, email, web_url, extra_links, retrieved_at FROM profiles;" > profiles.csv
